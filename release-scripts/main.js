/**
 * This file is an entrypoint for Electron.
 */
const { app, BrowserWindow, Menu } = require('electron');
const path = require('path');

app.on('ready', () => {
    let win = new BrowserWindow({
        width: 640,
        height: 480,
        fullscreen: true,
        center: true,
        webPreferences: {
            nodeIntegration: false
        }
    });
    Menu.setApplicationMenu(null);
    win.loadFile(path.join(__dirname, 'index.html'));
});
