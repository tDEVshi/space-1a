export default class Planet extends Phaser.Sprite
{
    constructor(game, x, y)
    {
        super(game, x, y,  'ss', 128);
        game.physics.arcade.enable(this); // fizyka
        game.world.add(this); // dodanie do świata
        this.body.collideWorldBounds = true;
        this.anchor.setTo(.5, .5);
        };
        
        
    update() {
        //przelot
        this.body.velocity.x=-Math.floor(Math.random() * 250) + 50;
        
        this.body.rotation-=5;
        this.blockTimer = 0;
        this.killed = false;
        if(this.body.blocked.left && this.game.time.now > this.blockTimer) {
            this.blockTimer = this.game.time.now + 750;
            this.killed = true;
            this.kill();
        }
        
    }
}