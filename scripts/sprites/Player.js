export default class Player extends Phaser.Sprite
{
    constructor(game, x, y)
    {
        super(game, x, y, 'ss', 1);
        game.physics.arcade.enable(this); // fizyka
        game.world.add(this); // dodanie do świata
         // animacje
         //this.animations.add('fly', [0,1, 2, 4, 5], 5, true);
         //this.animations.add('boost', [], 8, true);
         //this.play('fly');
         this.anchor.setTo(.5, .5);
         this.body.collideWorldBounds = true;
         this.body.width = 33;
        this.body.height = 20;
        this.body.offset.x =0;
        this.body.offset.y = 6;

        };
        
        
    update() {
        //spadanie
       if(this.body.velocity.y<250)
        this.body.velocity.y+=5;
        if(this.body.rotation<25)
        this.body.rotation+=1.5;
        
        // latanie
         if (
            this.game.input.keyboard.isDown(Phaser.Keyboard.UP)
            )
        {   
            if(this.body.velocity.y>-250)
            this.body.velocity.y -= 10;
            if(this.body.rotation>-25)
            this.body.rotation-=3;
            //this.play('fly');
            //this.animations.stop(null, true); // reset animacji
        }
    }
}