export default class SplashState extends Phaser.State
{
    preload()
    {
        this.game.stage.backgroundColor = '#FFFFFF';
    }
	create()
    {
		this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'logo')
            .anchor.set(0.5);
        this.game.time.events.add(Phaser.Timer.SECOND * 2, () => {
            this.game.state.start('GameState');
        });
	}
}