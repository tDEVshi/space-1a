export default class MenuState extends Phaser.State
{
    preload()
    {
        this.game.stage.backgroundColor = '#111111';
    }
	create()
    {
        
        this.game.add.text(this.game.width/2-3*32,this.game.height/2,'Lost in Space', {boundsAlignH: 'center' , fontSize: '40px', fill:'#FFFFFF'});
        this.game.add.text(this.game.width/2,this.game.height/2+50,'Press SPACE to Continue', {fontSize: '20px', fill:'#FFFFFF'});

        
    }
    update()
    {
        //if(this.game.input.keyboard.isDown(Phaser.KeyCode.SPACEBAR))
        //{
            this.game.state.start('GameState');
        //}
    }
}