import Player from '../sprites/Player';
import Planet from '../sprites/planet';
export default class GameState extends Phaser.State
{
    init(args)
    {
        args = args || {};
    }
    preload()
    {
        
    }
	create()
    {
        
        //Enable Arcade Physics
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        //Set the games background colour
        this.game.stage.backgroundColor = '#1d0e31';
        //wrogie "planety"
        this.planets = this.game.add.group();
        this.game.time.events.loop(1 * Phaser.Timer.SECOND, this.spawnPlanet, this);
        this.score=0;
        this.lives=3;
        //gracz
        this.player = new Player(this.game,100,this.game.height/2);


        this.scoreText=this.game.add.text(16,16,'Score: '+this.score, {boundsAlignH: 'center' , fontSize: '20px', fill:'#FFFFFF'});
        this.livesText=this.game.add.text(800-96,16,'Lives: '+this.lives, {boundsAlignH: 'center' , fontSize: '20px', fill:'#FFFFFF'});
        
	}
    update()
    {
        this.game.physics.arcade.overlap(this.player, this.planets, (player,planet) => {
                this.player.y=this.game.height/2;
                this.score-=500;
                this.lives-=1;
                this;
                planet.kill();
        })
        
        if(this.killed==true)
        {
            this.score+=100;
        }
        this.livesText=this.livesText.setText('Lives: '+this.lives);
        this.scoreText=this.scoreText.setText('Score: '+this.score);
        
    }    
    
    spawnPlanet()
    {
        this.planets.add(new Planet(this.game, 800+70,Math.floor(Math.random() * 600) + 1));
    }
    render()
    {
        //this.game.debug.body(this.player);    
    }
}