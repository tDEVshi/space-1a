import config from 'config.js';

export default class LoadingState extends Phaser.State
{
    preload()
    {
        this.game.load.image('logo', 'assets/images/logov2.png');
        this.game.load.spritesheet('ss', 'assets/images/spritesheet1.png', 32, 32);
        this.game.load.image('bg', 'assets/images/bg.png');
        this.game.stage.backgroundColor = '#CC7EEF';
    }
	create()
    {
        this.game.state.start(
            config.game.skipSplashState
                ? (config.game.skipMenuState ? 'GameState' : 'MenuState')
                : 'SplashState'
        );
	}
}