// config
import config from 'config.js';
// states
import BootState from 'states/BootState';
import LoadingState from 'states/LoadingState';
import SplashState from 'states/SplashState';
import MenuState from 'states/MenuState';
import GameState from 'states/GameState';
// dependencies
import Stats from 'stats.js';

class Game extends Phaser.Game
{
	constructor() {
		super(config.game.width, config.game.height, Phaser.AUTO, 'content', null); // creates a game
        this.ENV = '{{ENV}}'; // loaded through Gulp
        // adds states
        this.state.add('BootState', BootState, false);
        this.state.add('LoadingState', LoadingState, false);
		this.state.add('SplashState', SplashState, false);
		this.state.add('MenuState', MenuState, false);
		this.state.add('GameState', GameState, false);
        // turns on the performence statistics
        if (this.ENV === 'dev') {
            this.setupStatsJS();
        }
        // starts the boot state
        this.state.start('BootState');
	}
    setupStatsJS() {
        const stats = new Stats();
        document.body.appendChild(stats.dom);
        const updateLoop = this.update;
        this.update = (...args) => {
            stats.begin();
            updateLoop.apply(this, args);
            stats.end();
        };
    }
    resizeGame(width, height) {
        this.scale.setGameSize(width, height);
        config.game.width = width;
        config.game.height = height;
        resize();
    }
}

const game = new Game(); // starts a game

function resize() {
    let scalex = window.innerWidth / config.game.width;
    let scaley = window.innerHeight / config.game.height;
    if (config.game.pixelArtRendering) {
        scalex = Math.floor(scalex);
        scaley = Math.floor(scaley);
    }
    const scale = Math.min(scalex, scaley);
    game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
    game.scale.setUserScale(scale, scale);
    if (config.game.pixelArtRendering) {
        game.renderer.renderSession.roundPixels = true;
        Phaser.Canvas.setImageRenderingCrisp(game.canvas);
    }
    document.getElementById('content').className = 'resized';
    document.getElementById('content').style.marginLeft = '-' + Math.floor(config.game.width * scale / 2) + 'px';
    document.getElementById('content').style.marginTop = '-' + Math.floor(config.game.height * scale / 2) + 'px';
    document.getElementById('content-placeholder').style.width = (config.game.width * scale) + 'px';
    document.getElementById('content-placeholder').style.height = (config.game.height * scale) + 'px';
}﻿;
window.addEventListener('load', resize, false);
window.addEventListener('resize', resize, false);
