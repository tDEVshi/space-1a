export default {
    game: {
        width: 800,
        height: 600,
        skipSplashState: false,
        skipMenuState: false,
        pixelArtRendering: true
    }
};